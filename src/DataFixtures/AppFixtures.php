<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Offer;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private $passwordHasherEncoder;
    public function __construct(UserPasswordHasherInterface $passwordHasherEncoder)
    {
        $this->passwordHasherEncoder = $passwordHasherEncoder;
    }
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        // Fake offers
        for ($i=0; $i < 5; $i++) { 
            $offer = new Offer();
            $offer->setTitle($faker->title);
            $offer->setDescription($faker->text);
            $offer->setPrice($faker->randomDigit);
            $offer->setStatuts(true);
            $manager->persist($offer);
        }
        // Fake User
        for ($i = 0; $i < 9; $i++) {
            $user = new User();
            $user->setEmail($faker->email);
            $user->setPassword($this->passwordHasherEncoder->hashPassword($user, "password123"));
            $user->setName($faker->name);
            $user->setAddress($faker->address);
            $user->setPhoneNumber($faker->phoneNumber);
            $manager->persist($user);
        }
        // ADMIN
        $admin = new User();
        $admin->setEmail("admin@ps.mg");
        $admin->setPassword($this->passwordHasherEncoder->hashPassword($admin, "admin@ps.mg"));
        $admin->setName("Admin Admin");
        $admin->setAddress("Addresse Admin");
        $admin->setPhoneNumber("0398569241");
        $admin->setRoles(['ROLE_ADMIN']);
        $manager->persist($admin);
        $manager->flush();
    }
}
