<?php

namespace App\Controller;

use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class UserController extends AbstractController
{
    #[Route('/profile', name: 'app_user_profile')]
    public function index(): Response
    {
        $user = $this->getUser();
        return $this->render('client/profile.html.twig', compact('user'));
    }

    #[Route('/profile/edit', name: 'app_user_profile_edit')]
    public function edit(Request $request, EntityManagerInterface $entityManagerInterface): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
           $entityManagerInterface->flush();
           $this->addFlash('success',"Modification avec succèss");
           return $this->redirectToRoute('app_user_profile');
        }

        return $this->render('client/edit-profile.html.twig', [
            'user' => $user,
            'form' => $form->createView()
        ]);
    }
}
