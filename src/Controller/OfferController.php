<?php

namespace App\Controller;

use App\Entity\Offer;
use App\Entity\Subscribe;
use App\Repository\OfferRepository;
use App\Repository\SubscribeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/offer')]
class OfferController extends AbstractController
{

    #[Route('/', name: 'app_offer')]
    public function index(OfferRepository $offerRepository): Response
    {
        $offers = $offerRepository->getAvalaibleOffers();
        return $this->render('client/index.html.twig', compact('offers'));
    }


    // Historique 

    #[Route('/archive', name: 'app_offer_user_archive')]
    public function history(SubscribeRepository $subscribeRepository): Response
    {
        $subscribes = $subscribeRepository->getUserSubcription($this->getUser());
        return $this->render('client/archive.html.twig', compact('subscribes'));
    }

    // Souscrire

    #[Route('/{offer}/subscribe', name: 'app_offer_subcribe')]
    public function subscribe(Offer $offer, EntityManagerInterface $entityManagerInterface): Response
    {
        $subscription = new Subscribe();
        $user = $this->getUser();
        $subscription->setUser($user);
        $subscription->setOffer($offer);
        $entityManagerInterface->persist($subscription);
        $entityManagerInterface->flush();
        return $this->redirectToRoute('app_offer_subcribe_success');
    }


    #[Route('/subscribe/success', name: 'app_offer_subcribe_success')]
    public function subscribeSuccess(): Response
    {
        return $this->render('client/subscription-done.html.twig');
    }
}
