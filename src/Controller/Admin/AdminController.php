<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Repository\OfferRepository;
use App\Repository\SubscribeRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminController extends AbstractController
{
    #[Route('/admin', name: 'app_admin_home')]
    public function index(OfferRepository $offerRepository): Response
    {
        $offers = $offerRepository->getAvalaibleOffers();
        return $this->render('admin/dashboard.html.twig', [
            'activeMenu' => "",
            'offers' => $offers,
        ]);
    }

    #[Route('/admin/clients', name: 'app_admin_clients')]
    public function clients(UserRepository $userRepository): Response
    {
        $users = $userRepository->getClients();
        return $this->render('admin/users/clients.html.twig', [
            'activeMenu' => "clients",
            'users' => $users,
        ]);
    }

    #[Route('/admin/client/{id}', name: 'app_admin_client_show')]
    public function show(User $user): Response
    {
        return $this->render('admin/users/show.html.twig', [
            'activeMenu' => "clients",
            'user' => $user,
        ]);
    }

    #[Route('/admin/subscription/show', name: 'app_admin_subscription')]
    public function subscriptionShow(SubscribeRepository $subscribeRepository): Response
    {
        return $this->render('admin/subscriptions/list.html.twig', [
            'activeMenu' => "subscription",
            'subscriptions' => $subscribeRepository->findBy([], ['createdAt' => "DESC"] ),
        ]);
    }
}
