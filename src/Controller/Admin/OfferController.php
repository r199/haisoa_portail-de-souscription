<?php

namespace App\Controller\Admin;

use App\Entity\Offer;
use App\Form\OfferType;
use App\Repository\OfferRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/offer')]
class OfferController extends AbstractController
{
    #[Route('/', name: 'app_admin_offer_index', methods: ['GET'])]
    public function index(OfferRepository $offerRepository): Response
    {
        return $this->render('admin/offers/index.html.twig', [
            'offers' => $offerRepository->findAll(),
            'activeMenu' => "offres",
        ]);
    }

    #[Route('/new', name: 'app_admin_offer_new', methods: ['GET', 'POST'])]
    public function new(Request $request, OfferRepository $offerRepository): Response
    {
        $offer = new Offer();
        $form = $this->createForm(OfferType::class, $offer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $offerRepository->save($offer, true);
            $this->addFlash('success','Ajout avec succèss');
            return $this->redirectToRoute('app_admin_offer_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/offers/new.html.twig', [
            'offer' => $offer,
            'activeMenu' => "offres",
            'form' => $form,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_offer_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Offer $offer, OfferRepository $offerRepository): Response
    {
        $form = $this->createForm(OfferType::class, $offer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $offerRepository->save($offer, true);
            $this->addFlash('success', 'Modification avec succèss');
            return $this->redirectToRoute('app_admin_offer_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/offers/edit.html.twig', [
            'offer' => $offer,
            'form' => $form,
            'activeMenu' => "offres",
        ]);
    }

    #[Route('/{id}', name: 'app_admin_offer_delete', methods: ['POST'])]
    public function delete(Request $request, Offer $offer, OfferRepository $offerRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$offer->getId(), $request->request->get('_token'))) {
            $offerRepository->remove($offer, true);
            $this->addFlash('success','Suppression avec succèss');
        }

        return $this->redirectToRoute('app_admin_offer_index', [], Response::HTTP_SEE_OTHER);
    }
}
