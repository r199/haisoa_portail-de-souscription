# Portal Subscription

Portal Subscription : Haisoa (Développeé en Symfony/php)

## Requirements
    PHP : version 8 et plus
    Composer : version 2.3 et plus

## Installation

Vérifier si vous avez les extensions necessaires.Si non,Il faut installer (avec les dernières version) :
    - [Xampp](https://www.apachefriends.org/download.html).
    - [Composer](https://getcomposer.org/download/).
    - [Symfony](https://symfony.com/download).

Après avoir vérifié les éléments necessaires, Suivez les instructions suivantes :

### Installation des dépendances php nécessaires avec composer
```bash
composer install
```
##### NB 👍 : Pour l'utilisateur MySQL, veuillez configurer le fichier .env.

#### Mais vous pouvez aussi utiliser SQLite, en décommentant le 'DATABASE_URL="sqlite:///%kernel.project_dir%/var/data.db"' dans le fichier .env

### création DB, (si elle n'existe pas encore), 
```bash
    php bin/console d:d:c
```

## Utilisation

### Lançement du serveur, 
```bash
    symfony serve -d
```



### Migration
```bash
symfony console make:migration
symfony console d:m:m
```

### Génerer les fixtures (fausses données)
```bash
symfony console d:f:l
```

### Lien accessible de notre projet
[Cliquez-ici](http://127.0.0.1:8000).






### Compte par defaut ADMIN
[Lien vers Administrateur](http://127.0.0.1:8000/admin).
```bash
    e-mail : admin@ps.mg
    mdp : admin@ps.mg
```




## Les API
- API Users : [Cliquez-ici](http://127.0.0.1:8000/api/users).
- API Offres : [Cliquez-ici](http://127.0.0.1:8000/api/offers).
- API Souscriptions : [Cliquez-ici](http://127.0.0.1:8000/api/subscribes).


