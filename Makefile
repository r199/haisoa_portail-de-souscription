start-server:
	docker-compose up -d
	symfony serve -d
	symfony open:local
.PHONY: start-server

migration:
	symfony console make:migration
	symfony console d:m:m
.PHONY: migration

stop-server:
	docker-compose down --remove-orphans
	symfony server:stop
.PHONY: stop-server


